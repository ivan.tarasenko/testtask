﻿using System;
using System.Collections.Generic;

namespace CSharpTest
{
    public class WorkDayCalculator : IWorkDayCalculator
    {
        private static WorkDayCalculator _instance;
        public static WorkDayCalculator Instance
        {
            get => _instance ?? (_instance = new WorkDayCalculator());
        }
        
        public DateTime Calculate(DateTime startDate, int dayCount, IEnumerable<WeekEnd> weekEnds)
        {
            var end = startDate.AddDays(dayCount - 1);

            if (weekEnds != null)
            {
                foreach (var weekEnd in weekEnds)
                {
                    if (end >= weekEnd.StartDate)
                    {
                        var timeSpan = weekEnd.EndDate - weekEnd.StartDate;
                        end = end.AddDays(timeSpan.Days + 1);
                    }
                    else
                    {
                        break;
                    }
                }
            }

            return end;
        }

        private WorkDayCalculator()
        {
        }
    }
}
