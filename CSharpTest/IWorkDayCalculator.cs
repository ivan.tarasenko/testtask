﻿using System;
using System.Collections.Generic;

namespace CSharpTest
{
    public interface IWorkDayCalculator
    {
        DateTime Calculate(DateTime startDate, int dayCount, IEnumerable<WeekEnd> weekEnds);
    }
}
