﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CSharpTest
{
    [TestClass]
    public class WorkDayCalculatorTests
    {
        [TestMethod]
        public void TestNoWeekEnd()
        {
            var startDate = new DateTime(2014, 12, 1);
            var count = 10;

            var result = WorkDayCalculator.Instance.Calculate(startDate, count, null);

            Assert.AreEqual(startDate.AddDays(count - 1), result);
        }

        [TestMethod]
        public void TestNormalPath()
        {
            var startDate = new DateTime(2017, 4, 21);
            var count = 5;
            var weekends = new WeekEnd[1]
            {
                new WeekEnd(new DateTime(2017, 4, 23), new DateTime(2017, 4, 25))
            };

            var result = WorkDayCalculator.Instance.Calculate(startDate, count, weekends);

            Assert.IsTrue(result.Equals(new DateTime(2017, 4, 28)));
        }
        
        [TestMethod]
        public void TestTwoWeekEnds()
        {
            var startDate = new DateTime(2017, 4, 21);
            var count = 5;
            var weekends = new WeekEnd[2]
            {
                new WeekEnd(new DateTime(2017, 4, 23), new DateTime(2017, 4, 25)),
                new WeekEnd(new DateTime(2017, 4, 28), new DateTime(2017, 4, 29))
            };

            var result = WorkDayCalculator.Instance.Calculate(startDate, count, weekends);

            Assert.IsTrue(result.Equals(new DateTime(2017, 4, 30)));
        }

        [TestMethod]
        public void TestWeekendAfterEnd()
        {
            var startDate = new DateTime(2017, 4, 21);
            var count = 5;
            var weekends = new WeekEnd[2]
            {
                new WeekEnd(new DateTime(2017, 4, 23), new DateTime(2017, 4, 25)),
                new WeekEnd(new DateTime(2017, 4, 29), new DateTime(2017, 4, 29))
            };

            var result = WorkDayCalculator.Instance.Calculate(startDate, count, weekends);

            Assert.IsTrue(result.Equals(new DateTime(2017, 4, 28)));
        }
    }
}